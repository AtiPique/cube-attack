# Cube Attack ! 

### A game written in Python 🐍

#### The goal of the game is accumulate points by collecting coins 
#### and dodging the other square that attack you. 

# License 

![gplV3](./gplv3.png)