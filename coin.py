import pygame 
from pygame.locals import *

class Coin:
    def __init__(self, fenetre, xy):
        self.img = pygame.image.load("assets/coin.png")
        self.pos = self.img.get_rect()
        self.pos.x = xy[0]
        self.pos.y = xy[1]
        self.fenetre = fenetre
        
    def afficher(self):
        self.fenetre.blit(self.img, self.pos)        
