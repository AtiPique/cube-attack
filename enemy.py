import pygame
from pygame.locals import *

# Classe ennemi

class Enemy:
    
    
    def __init__(self, fenetre, speed, xy):
        self.img = pygame.image.load("assets/sprite.jpg")
        self.hide = pygame.image.load("assets/hide.jpg")
        self.pos = self.img.get_rect()
        self.fenetre = fenetre
        self.speed = speed
        self.xy = xy
        self.pos.x = xy[0]
        self.pos.y = xy[1]

    def afficher(self):
        self.fenetre.blit(self.img, self.pos)
         
    def left_to_right(self):
        self.pos.x += self.speed # mouvement de gauche à droite.
            
    def right_to_left(self):
        self.pos.x -= self.speed # mouvement de droite à gauche.    
    
    def up_to_down(self):
        self.pos.y += self.speed # mouvement de haut en bas.      
            
    def down_to_up(self):
        self.pos.y -= self.speed # mouvement de bas en haut.         
    


    


