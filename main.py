from player import Player
from enemy import Enemy
from coin import Coin

import pygame
from pygame.locals import *
from random import randint

pygame.init()

window = pygame.display.set_mode((800, 600))
bg = pygame.image.load("assets/background.jpg")
pygame.display.set_caption("Cube Attack")

player = Player(window)
running = True
coin = True
enemy = True
score = 0 

def key_parser(key):
    
    '''
    Permet de choisr le mouvment a effectuer selon 
    la touche appuyée par l'utilisateur. 
    '''
    
    if key == pygame.K_UP:
        player.up()
    elif key == pygame.K_DOWN:
        player.down()
    elif key == pygame.K_RIGHT:
        player.right()
    elif key == pygame.K_LEFT:
        player.left()

def genEnemy():
    
    '''
    Permet de choisir le point de départ de l'enemi selon
    le chiffre renvoyé par la fonction randint(). 
    '''
    
    global enemy # permet d'accèder a la classe en dehors de la fonction.
    
    rand = randint(0, 3) # assigne le chiffre retrourné par la fonction randint() a la variable rand

    if score > 50:
        if rand == 0:
            enemy = Enemy(window, 1, (0, 292)) 
        elif rand == 1:
            enemy = Enemy(window, 1, (0, 292)) # (800, 292) not working
        elif rand == 2:
            enemy = Enemy(window, 1, (392, 0))
        elif rand == 3:
            enemy = Enemy(window, 1, (392, 599)) # (392, 600) not working
    else:
        if rand == 0:
            enemy = Enemy(window, 0.5, (0, 292))
        elif rand == 1:
            enemy = Enemy(window, 0.5, (0, 292)) # (800, 292) not working
        elif rand == 2:
            enemy = Enemy(window, 0.5, (392, 0))
        elif rand == 3:
            enemy = Enemy(window, 0.5, (392, 0)) # (392, 600) not working
    
    
def genCoin():
    
    '''
    Génère la prochaine pièce, selon le chiffre retourné par 
    la fonction randint les coordonnée seront différentes
    '''
    
    global coin # permet d'accèder a la classe en dehors de la fonction. 
    
    rand = randint(0, 3)

    if rand == 0:
        coin = Coin(window, (328, 292))
    elif rand == 1:
        coin = Coin(window, (456, 292))
    elif rand == 2:
        coin = Coin(window, (392, 228))
    elif rand == 3:
        coin = Coin(window, (392, 356))

while running == True:
    if enemy == True:
        genEnemy()
    
    while (enemy.pos.x, enemy.pos.y) != (392, 292):
            
        if coin == True:
            genCoin()
    
        if enemy != True:
            if enemy.xy[0] == 0:
                enemy.left_to_right()

            if enemy.xy[0] == 799:
                enemy.right_to_left()

            if enemy.xy[1] == 0:
                enemy.up_to_down()

            if enemy.xy[1] == 599:
                enemy.down_to_up() 

            if (enemy.pos.x, enemy.pos.y) == (player.pos.x, player.pos.y):
                del enemy
                del player
                print("Perdu !")
                print(f"{score} points !")
                pygame.quit()


        window.blit(bg, (1, 1))

        if coin != True:
            coin.afficher()

        if enemy != True :
            enemy.afficher()
        
        player.afficher()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.KEYDOWN:
                key_parser(event.key)
            else:
                player.center()

        try:
            if player.pos == coin.pos:
                score = score + 2
                del coin
                coin = True
        except:
            print("")

        pygame.time.delay(1)
        pygame.display.flip()
    
    del enemy
    enemy = True
    score = score + 1
