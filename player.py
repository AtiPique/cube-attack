import pygame
from pygame.locals import *

center = (392, 292) # tuple des coordonées du joueur au centre de la fenetre. 

# Classe du joueur

class Player:
    
    def __init__(self, fenetre):
        self.img = pygame.image.load("assets/sprite.jpg")
        self.pos = self.img.get_rect()
        self.fenetre = fenetre
        self.speed = 64
        self.pos.x = 392
        self.pos.y = 292
        
    def center(self):
        self.pos.x = 392
        self.pos.y = 292
    
    def left(self):
        if (self.pos.x, self.pos.y) == center:
            self.pos.x -= self.speed

    def right(self):
        if (self.pos.x, self.pos.y) == center:
            self.pos.x += self.speed

    def up(self):
        if (self.pos.x, self.pos.y) == center:
            self.pos.y -= self.speed

    def down(self):
        if (self.pos.x, self.pos.y) == center:
            self.pos.y += self.speed
 
    def afficher(self):
        self.fenetre.blit(self.img, self.pos)